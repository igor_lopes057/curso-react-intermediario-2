/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/no-var-requires */
const graphql = require('graphql');

const { GraphQLObjectType, GraphQLInt, GraphQLString } = graphql;

const OrderType = new GraphQLObjectType({
  name: 'Order',
  fields: () => ({
    id: { type: GraphQLInt },
    name: { type: GraphQLString },
    lastName: { type: GraphQLString },
    phone: { type: GraphQLString },
    order: { type: GraphQLString },
    description: { type: GraphQLString },
  }),
});

module.exports = OrderType;
