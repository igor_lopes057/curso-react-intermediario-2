/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable @typescript-eslint/no-var-requires */
const graphql = require('graphql');
const OrdersType = require('./TypeDefs/OrdersType.js');

const { GraphQLSchema, GraphQLObjectType, GraphQLList, GraphQLInt, GraphQLString } = graphql;

const ordersData = require('../db.json');

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    getAllOrders: {
      type: new GraphQLList(OrdersType),
      args: { id: { type: GraphQLInt } },
      resolve(parent, args) {
        // Acesso a um DB.
        return ordersData;
      },
    },
  },
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    createOrder: {
      type: OrdersType,
      args: {
        name: { type: GraphQLString },
        lastName: { type: GraphQLString },
        phone: { type: GraphQLString },
        order: { type: GraphQLString },
        description: { type: GraphQLString },
      },
      resolve(parent, args) {
        OrdersType.push({
          id: OrdersType.length + 1,
          name: args.nome,
          lastName: args.lastName,
          phone: args.phone,
          order: args.order,
          description: args.description,
        });
        return args;
      },
    },
  },
});

module.exports = new GraphQLSchema({ query: RootQuery, mutation: Mutation });
