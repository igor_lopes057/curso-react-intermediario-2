import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ApolloClient, InMemoryCache, ApolloProvider, HttpLink, from } from '@apollo/client';

import { ToastProvider } from 'context/ToastContext';
import { Toast } from 'components/Toast';

const Order = React.lazy(() => import('./pages/Order'));
const Home = React.lazy(() => import('./pages/Home'));

const link = from([new HttpLink({ uri: 'http://localhost:3001/graphql' })]);
const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <React.Suspense fallback="Loading...">
          <ApolloProvider client={client}>
            <ToastProvider>
              <Route path="/">
                <Home />
              </Route>
              <Route path="/orders">
                <Order />
              </Route>
              <Toast />
            </ToastProvider>
          </ApolloProvider>
        </React.Suspense>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
