import React from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, Grid, Theme, Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme: Theme) => ({
  toolbar: {
    padding: '0 1.5rem',
    flexWrap: 'inherit',
  },
  contentToolbar: {
    display: 'flex',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      marginLeft: 'auto',
    },
  },
  link: {
    color: 'white',
  },
}));

export const Header = (): JSX.Element => {
  const classes = useStyles();

  return (
    <AppBar position="static" color="primary" elevation={0}>
      <Toolbar className={classes.toolbar}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          style={{ flexWrap: 'inherit' }}
        >
          <h4> Pizzaria </h4>
          <Box>
            <Link className={classes.link} to="/" style={{ marginRight: '1rem' }}>
              Home
            </Link>
            <Link className={classes.link} to="/orders">
              Pedidos
            </Link>
          </Box>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};
