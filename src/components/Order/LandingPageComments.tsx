import React from 'react';
import { Avatar, Typography, Box, Stack, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface LandingPageCommentsProps {
  users: { avatar: string; name: string; comment: string }[];
}

const useStyles = makeStyles({
  card: {
    background: '#F0FFF0',
    width: '300px',
    borderRadius: '8px',
    boxShadow: '1px 2px 0px rgba(0,0,0, 0.25)',
    marginBottom: '2rem',
    minHeight: '190px',
  },
  userInfo: {
    padding: '1rem',
    background: '#5B2F8B',
    borderTopLeftRadius: '8px',
    borderTopRightRadius: '8px',
  },
});

export const LandingPageComments = ({ users }: LandingPageCommentsProps) => {
  const classes = useStyles();
  return (
    <>
      {users.map((user) => (
        <Grid item xs={3} key={user.name}>
          <Box className={classes.card}>
            <Stack direction="row" alignItems="center" spacing={5} className={classes.userInfo}>
              <Avatar alt="Photo User" src={user.avatar} sx={{ border: '2px solid white' }} />
              <Typography sx={{ color: 'white' }}>{user.name}</Typography>
            </Stack>
            <Typography sx={{ padding: '2rem 1rem' }}>{user.comment}</Typography>
          </Box>
        </Grid>
      ))}
    </>
  );
};
