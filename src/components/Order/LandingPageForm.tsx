import React from 'react';
import { Field, Form, Formik } from 'formik';
import * as yup from 'yup';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { TextFieldMask, TextFieldWrapper } from 'components/Fields';
import { Orders } from 'types/orders';
import { SubmitButton } from './SubmitButton';

const useStyles = makeStyles({
  form: {
    marginTop: '32px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  button: {
    height: '3.5rem',
    fontSize: '0.9rem',
  },
});

const validationSchema: yup.SchemaOf<Orders> = yup.object({
  id: yup.number(),
  name: yup.string().required('Campo obrigatório'),
  lastName: yup.string().required('Campo obrigatório'),
  phone: yup.string().required('Campo obrigatório'),
  order: yup.string().required('Campo obrigatório'),
  description: yup.string(),
});

interface LandingPageFormProps {
  onSubmit: (data: Orders) => void;
  initialValues: Orders;
}
export const LandingPageForm = ({ onSubmit, initialValues }: LandingPageFormProps): JSX.Element => {
  const classes = useStyles();
  return (
    <Formik enableReinitialize initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
      {({ isValid }) => (
        <Form className={classes.form}>
          <Field
            id="name"
            name="name"
            type="name"
            label="Nome"
            placeholder="Informe seu nome"
            component={TextFieldWrapper}
          />
          <Field
            id="lastName"
            name="lastName"
            label="Sobrenome"
            type="lastName"
            placeholder="Informe seu sobrenome"
            component={TextFieldWrapper}
          />
          <Field
            component={TextFieldMask}
            name="phone"
            label="Telefone"
            variant="outlined"
            placeholder="Insira um telefone"
          />
          <Field
            id="order"
            label="Informe seu pedido"
            name="order"
            type="order"
            placeholder="Insira o pedido"
            component={TextFieldWrapper}
          />
          <Field
            id="description"
            name="description"
            label="Descrição"
            type="description"
            placeholder="Insira a descrição. Ex: Sem cebola"
            component={TextFieldWrapper}
          />
          <SubmitButton disabled={!isValid} buttonProps={{ className: classes.button }} />
        </Form>
      )}
    </Formik>
  );
};
