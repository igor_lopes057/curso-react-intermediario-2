import React from 'react';
import { Typography, Box, Stack, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Orders as IOrders } from 'types/orders';

const useStyles = makeStyles(() => ({
  comment: {
    lineHeight: '2.3rem',
    color: '#5b5b5b',
    fontWeight: 600,
  },
  card: {
    background: '#F0FFF0',
    width: '300px',
    borderRadius: '8px',
    boxShadow: '1px 2px 0px rgba(0,0,0, 0.25)',
    minHeight: '175px',
  },
  userInfo: {
    padding: '1rem',
    background: '#5B2F8B',
    borderTopLeftRadius: '8px',
    borderTopRightRadius: '8px',
  },
}));

interface OrdersProps {
  orders: IOrders[];
}

export const Orders = ({ orders }: OrdersProps): JSX.Element => {
  const classes = useStyles();

  return (
    <section>
      <Typography variant="h4" sx={{ mt: '3rem', lineHeight: '2.3rem', color: '#5b5b5b', fontWeight: 500, pb: '3rem' }}>
        Listagem de Pedidos
      </Typography>

      <Grid container alignItems="center">
        {orders ? (
          orders.map((item: IOrders) => {
            return (
              <Box className={classes.card} key={item.id} sx={{ margin: '0.5rem' }}>
                <Stack direction="row" alignItems="center" spacing={5} className={classes.userInfo}>
                  <Typography sx={{ color: 'white' }}>{`${item.name} ${item.lastName}`}</Typography>
                </Stack>
                <Typography sx={{ padding: '2rem 1rem' }}>
                  <strong>Pedido: </strong>
                  {item.order}
                  <br />
                  {item.description && <strong>Descrição:</strong>} {item.description}
                </Typography>
              </Box>
            );
          })
        ) : (
          <Grid container alignItems="center" justifyContent="space-around" sx={{ margin: '2rem 0' }}>
            <Typography variant="h6" className={classes.comment}>
              Nenhum pedido encontrado!
            </Typography>
          </Grid>
        )}
      </Grid>
    </section>
  );
};
