import React from 'react';
import { storiesOf } from '@storybook/react';

import { SubmitButton } from './SubmitButton';

storiesOf('Components', module).add('SubmitButtonSample', () => {
  return <SubmitButton disabled={false} />;
});
