import React from 'react';
import { Button } from '@mui/material';

interface SubmitButtonProps {
  disabled: boolean;
  buttonProps?: object;
}

export const SubmitButton = ({ disabled, buttonProps }: SubmitButtonProps): JSX.Element => {
  return (
    <Button
      disabled={disabled}
      variant="contained"
      type="submit"
      autoFocus
      disableElevation
      sx={{ mt: 2 }}
      {...buttonProps}
    >
      Enviar
    </Button>
  );
};
