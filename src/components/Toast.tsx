import React, { useContext } from 'react';
import { Alert, Button, Snackbar, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { ToastContext } from 'context/ToastContext';

const useStyles = makeStyles((theme: Theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  contentAlert: {
    display: 'flex',
    flex: 1,
  },
  ContainersnackBar: {
    display: 'flex',
    justifyContent: 'space-between',
    [theme.breakpoints.up('sm')]: {
      width: '557px',
    },
  },
}));
export function Toast(): JSX.Element {
  const classes = useStyles();
  const { isOpen, handleClose, message, severityColor } = useContext(ToastContext);

  return (
    <div className={classes.root}>
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={isOpen}
        onClose={handleClose}
        className={classes.ContainersnackBar}
        autoHideDuration={2500}
      >
        <Alert
          icon={false}
          severity={severityColor}
          variant="filled"
          action={
            <Button color="inherit" size="small" onClick={handleClose}>
              Fechar
            </Button>
          }
          className={classes.contentAlert}
        >
          {message}
        </Alert>
      </Snackbar>
    </div>
  );
}
