import { gql } from '@apollo/client';

export const CREATE_ORDER_MUTATION = gql`
  mutation createOrder($name: String!, $lastName: String!, $phone: String!, $order: String!, $description: String!) {
    createOrder(name: $name, lastName: $lastName, phone: $phone, order: $order, description: $description) {
      name
      lastName
      phone
      order
      description
    }
  }
`;
