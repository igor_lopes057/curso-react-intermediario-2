import { gql } from '@apollo/client';

export const LOAD_ORDERS = gql`
  query {
    getAllOrders {
      id
      name
      lastName
      phone
      order
      description
    }
  }
`;
