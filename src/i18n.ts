import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import { EN, PT } from 'resources';

const browserLng = (navigator.languages && navigator.languages[0]) || navigator.language;
i18n.use(initReactI18next).init({
  lng: browserLng,
  fallbackLng: 'pt',
  debug: false,
  supportedLngs: ['en', 'pt'],
  load: 'languageOnly',
  interpolation: {
    escapeValue: false,
  },
  resources: {
    en: {
      translation: {
        ...EN,
      },
    },
    pt: {
      translation: {
        ...PT,
      },
    },
  },
});

export default i18n;
