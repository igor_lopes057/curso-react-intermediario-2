import React from 'react';
import ReactDOM from 'react-dom';
import { CssBaseline, StyledEngineProvider, ThemeProvider } from '@mui/material';
import App from 'App';
import { theme } from 'config/material-ui';
import reportWebVitals from './reportWebVitals';
import './index.css';
import './i18n';

ReactDOM.render(
  <React.StrictMode>
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </StyledEngineProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
