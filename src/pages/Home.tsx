import React from 'react';
import { Divider, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Header } from 'components/Header';
import { LandingPageComments } from 'components/Order';
import LogoPizza1 from 'assets/logo1.jpg';
import LogoPizza2 from 'assets/logo2.jpg';
import { useTranslation } from 'react-i18next';
import i18next from 'i18n';

const useStyles = makeStyles(() => ({
  main: {
    padding: '0 0 2rem',
    margin: '0 1.5rem',
  },
  img: {
    borderRadius: '15px',
    marginTop: '3rem',
  },
  comment: {
    lineHeight: '2.3rem',
    color: '#5b5b5b',
    fontWeight: 600,
  },
}));

const usersCommentsMock = [
  {
    name: 'Marcelo Dias',
    avatar:
      'https://images.unsplash.com/photo-1599566150163-29194dcaad36?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=387&q=80',
    comment: 'Muito boa a pizzaria, recomendo a todos!',
  },
  {
    name: 'Juliana Matos',
    avatar:
      'https://media.istockphoto.com/photos/excited-woman-wearing-rainbow-cardigan-picture-id1327495437?b=1&k=20&m=1327495437&s=170667a&w=0&h=Vbl-XLyAnBoTkyGXXi-X1CFzuSHlNcn-dqB-sCosxFo=',
    comment: '5 estrelas, gosto muito, pertinho de casa',
  },
  {
    name: 'Thiago Gonçalves',
    avatar:
      'https://media.istockphoto.com/photos/young-man-is-playing-with-a-dog-and-do-selfie-picture-id1300658241?b=1&k=20&m=1300658241&s=170667a&w=0&h=0lrTViinfnDjbWDgxV0TDDSAXvzSgmrN-pKq0q60hqA=',
    comment: i18next.t('CommentExample'),
  },
];

const Home = (): JSX.Element => {
  const classes = useStyles();
  const { t } = useTranslation();

  return (
    <>
      <Header />
      <main className={classes.main}>
        <Grid item xs={12} container alignItems="center" sx={{ p: '3rem 2rem 0 0' }}>
          <Typography variant="h4" sx={{ lineHeight: '2.3rem', color: '#5b5b5b', fontWeight: 500, pb: '3rem' }}>
            {t('MakeYourOrder')}
          </Typography>
          <Typography variant="h6" sx={{ lineHeight: '2.3rem', color: '#5b5b5b' }}>
            {t('MakeYourOrderDescription')}
          </Typography>
          <Grid container justifyContent="space-evenly">
            <img className={classes.img} src={LogoPizza1} alt="Pizza" width="300px" />
            <img className={classes.img} src={LogoPizza2} alt="Pedaço de pizza" width="280px" />
          </Grid>
        </Grid>
        <Divider sx={{ m: '2rem 0 1rem' }} />
        <Typography variant="h6" className={classes.comment}>
          {t('Comment', { count: usersCommentsMock.length })}
        </Typography>

        <Grid container alignItems="center" sx={{ margin: '2rem 0' }}>
          <LandingPageComments users={usersCommentsMock} />
        </Grid>
      </main>
    </>
  );
};

export default Home;
