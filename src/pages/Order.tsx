import React, { useEffect, useState, useContext } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { Divider, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Header } from 'components/Header';
import { LandingPageForm } from 'components/Order';
import { Orders as IOrders } from 'types/orders';
import { LOAD_ORDERS } from 'graphql/Queries';
import { CREATE_ORDER_MUTATION } from 'graphql/Mutations';
import { ToastContext } from 'context/ToastContext';
import { Orders } from 'components/Order/Orders';

const useStyles = makeStyles(() => ({
  main: {
    padding: '0 0 1rem',
    margin: '0 2rem',
  },
  img: {
    borderRadius: '15px',
  },
  comment: {
    lineHeight: '2.3rem',
    color: '#5b5b5b',
    fontWeight: 600,
  },
}));

const INITIAL_VALUES = {
  name: '',
  lastName: '',
  phone: '',
  order: '',
  description: '',
};

const Order = (): JSX.Element => {
  const classes = useStyles();
  const { onMessageError, onMessageSuccess } = useContext(ToastContext);
  const [content, setContent] = useState<IOrders[]>([]);
  const [createOrder, { error: err }] = useMutation(CREATE_ORDER_MUTATION);
  const { data } = useQuery(LOAD_ORDERS);

  useEffect(() => {
    if (data) {
      setContent(data.getAllOrders);
    }
  }, [data]);

  const addOrder = (order: IOrders) => {
    createOrder({
      variables: {
        name: order.name,
        lastName: order.lastName,
        phone: order.phone,
        order: order.order,
        description: order.description,
      },
    });
    setContent([...content, order]);
    if (err) {
      onMessageError();
    } else {
      onMessageSuccess('Pedido cadastrado com sucesso');
    }
  };

  return (
    <>
      <Header />
      <main className={classes.main}>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item xs={6}>
            <img
              className={classes.img}
              src="https://media.istockphoto.com/photos/bakery-chef-prepare-pizza-picture-id1291299956?b=1&k=20&m=1291299956&s=170667a&w=0&h=Ys_FLtdY0Uzc7yTQl6JzvCHTQ3eRAuqNNU4x8EX1FB8="
              alt="Pizza"
            />
          </Grid>
          <Grid item xs={6}>
            <LandingPageForm onSubmit={addOrder} initialValues={INITIAL_VALUES} />
          </Grid>
        </Grid>
        <Divider sx={{ m: '2rem 0 1rem' }} />
        <Orders orders={content} />
      </main>
    </>
  );
};

export default Order;
