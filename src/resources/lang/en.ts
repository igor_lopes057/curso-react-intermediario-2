export const EN = {
  Comment_one: 'Comment',
  Comment_other: 'Comments',
  CommentExample: 'Extreme quality!',
  MakeYourOrder: 'Place your order with us',
  MakeYourOrderDescription:
    "Order at the best pizza place near you! Various flavors, fast delivery and with accessible places close to your home! We love what we do and we know that Brazilians are passionate about pizza. That's why our mission is to sell pizza with the best quality and the best working environments for our employees while pleasing your taste buds!",
};
