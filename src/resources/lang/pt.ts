export const PT = {
  Comment_one: 'Comentário',
  Comment_other: 'Comentários',
  CommentExample: 'Extrema qualidade!',
  MakeYourOrder: 'Faça seu pedido conosco',
  MakeYourOrderDescription:
    'Faça o pedido na melhor pizzaria perto de você! Diversos sabores, rapida entrega e com locais acessiveis pertinho ai da sua casa! A gente ama o que faz e sabe que o brasileiro é apaixonado por pizzas. Por isso, nossa missão é vender pizza com a melhor qualidade e garantindo os melhores ambientes de trabalho para nossos funcionários enquanto agrada o seu paladar!',
};
