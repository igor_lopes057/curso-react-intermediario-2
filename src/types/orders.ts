export interface Orders {
  id?: number;
  name: string;
  lastName: string;
  phone: string;
  order: string;
  description?: string;
}
